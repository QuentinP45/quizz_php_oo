<?php

$answerByField = [
    'label_formation' => 'LP informatique',
    'ville_formation' => 'Orléans',
    'date' => date("Y-m-d"),
    'humeur' => 5,
    'ville_favorite' => 'Orléans',
    'langages' => ['Php','Java','Python'],
    'parcours2' => 'Poursuite en entreprise'
];
