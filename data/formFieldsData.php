<?php

$formFieldsData = [
    [
		'type' => 'text',
        'label' => 'Quelle est votre formation ?',
        'id' => 'label_formation',
        'options' => [
            'pattern' => null, 
            'required' => true, 
            'placeholder' => 'LP informatique'
        ],
    ],
    [
		'type' => 'text',
        'label' => 'Quelle est votre ville de formation ?',
        'id' => 'ville_formation',
        'options' => [
            'pattern' => '[A-Z]{1}[a-zé]{3,}',
            'required' => true,
            'placeholder' => null
        ]
	],
	[
		'type' => 'email',
        'label' => 'Quelle est votre email ?',
        'id' => 'email',
        'options' => [
            'pattern' => null, 
            'required' => true, 
            'placeholder' => 'votre.mail@mail.com'
        ]
    ],
    [
		'type' => 'tel',
        'label' => 'Quel est votre numéro ?',
        'id' => 'contact',
        'options' => [
            'pattern' => null, 
            'required' => true, 
            'placeholder' => '06.xx.xx.xx.xx'
        ]
    ],
    [
		'type' => 'date',
        'label' => 'Quel est la date du jour ?',
        'id' => 'date',
        'options' => [
            'pattern' => null, 
            'required' => true, 
            'placeholder' => 'dd/mm/aaaa'
        ]
    ],
    [
		'type' => 'range',
        'label' => 'Quelle est votre humeur (de 1 à 5)',
        'id' => 'humeur',
        'options' => [
            'pattern' => null, 
            'required' => null, 
            'placeholder' => null
        ],
        'min' => 1,
        'max' => 5
    ],
    [
		'type' => 'select',
        'label' => 'Quelle est votre ville préférée ?',
        'id' => 'ville_favorite',
        'options' => [
            'pattern' => null, 
            'required' => null, 
            'placeholder' => null
        ],
        'option_values' =>[
            'Orléans',
            'Tours'
        ],
        'multiple' => false
    ],
    [
		'type' => 'select',
        'label' => 'Quels langages maitrisez vous ?',
        'id' => 'langages',
        'options' => [
            'pattern' => null, 
            'required' => null, 
            'placeholder' => null
        ],
        'option_values' =>[
            'Php',
            'Java',
            'Python'
        ],
        'multiple' => true
    ],
    [
		'type' => 'checkbox',
        'label' => 'Quels parcours envisagez vous suite à cette année ?',
        'id' => 'parcours',
        'options' => [
            'pattern' => null, 
            'required' => null, 
            'placeholder' => null
        ],
        'options_values' =>[
            'Miage',
            'Poursuite en entreprise',
        ]
    ],
];
