<?php 
    require_once 'libs/FormManager.php';
    require_once 'data/formFieldsData.php';

    $fm = new FormManager();

    if($_SERVER['REQUEST_METHOD']=='POST'){
        require_once 'libs/FormHandler.php';
        require_once 'data/answerByField.php';

        $handler = new FormHandler($_POST, $answerByField);
        $handler->checkResponses();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Quizz LP</title>
</head>
<body>
    <div class="container">
        <div class="p-3 mb-2 bg-primary text-white">
            <h1 class="text-center">Quizz Php</h1>
        </div>
        <div class="row">
        <div class="col-md-6">
            <?= $fm->openForm('POST') ?>
            <?php foreach($formFieldsData as $data): ?>
                <?php switch($data['type']):
                    case 'range' : ?>
                        <?php $fm->createRangeField($data['type'], $data['label'], $data['id'], $data['options'], $data['min'], $data['max']); ?>
                        <?php break ?>
                    <?php case 'select' : ?>
                        <?php $fm->createSelectField($data['type'], $data['label'], $data['id'], $data['options'], $data['option_values'], $data['multiple']); ?>
                        <?php break ?>
                    <?php case 'checkbox' : ?>
                        <?php $fm->createCheckboxField($data['type'], $data['label'], $data['id'], $data['options'], $data['options_values']); ?>
                        <?php break ?>
                    <?php default: ?>
                        <?php $fm->createBasicField($data['type'], $data['label'], $data['id'], $data['options']); ?>
                        <?php break ?>
                <?php endswitch; ?>
            <?php endforeach; ?>

            <?= $fm->renderForm() ?>

            <?= $fm->closeForm() ?>
        </div>
        <div class="col-md-6">
            <?php if($_SERVER['REQUEST_METHOD']=='GET') : ?>
                <h2>Merci de répondre au quizz</h2>
                <p class="text-primary">
                    Attention, la notation de ce quizz est totalement subjective et votre avenir professionnel ce retrouverait fortement 
                    compromis en cas d'échec !
                </p>
            <?php else : ?>
                <?php switch(true) :
                    case ($handler->getScore() < 9) : ?>
                        <h2>Votre score n'est pas le score maximal attendu... : <?= $handler->getScore() ?>/9</h2>
                        <p class="text-danger">
                            Vous venez d'échouer, vous n'avez vraisemblablement pas toutes les qualités humaines et techniques pour être un bon informaticien !
                        </p>
                        <p>
                            Vous avez la possibilité de repasser le test !
                        </p>
                        <?php break ?>
                    <?php default: ?>
                        <h2>Votre score est incroyable ! <?= $handler->getScore() ?>/9</h2>
                        <p class="text-success">
                            Vous avez réussis ce test, vous avez toutes les qualités requises pour être un bon informaticien !
                        </p>
                        <p>
                            Hum... rien n'est acquis, ne vous reposez pas sur vos lauriers
                        </p>
                        <?php break ?>
                <?php endswitch; ?>
            <?php endif; ?>
        </div>
        </div>
    </div>
</body>

</html>