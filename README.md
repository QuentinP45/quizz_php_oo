# Quizz Php Orienté Objet

## Architecture du projet

**data**

* answerByField
Contient les réponses attendues pour chacun des champs du quizz. Structure de données : tableau associatif (clé : name, valeur : texte ou liste de textes).

* formFieldsData
Contient les données à passer en paramètre de la méthode create[Type]Field() (création de champ par type) de l'objet FormManager.

**inputTypes**

* BasicInput
Objet  pour créer les champs "basiques" (label, id, name, type, attributs optionnels : required, etc). Prend en compte les champs de type text, date, tel, email, date.

* CheckboxInput, RangeInput, SelectInput
Objets héritants de BasicInput, prend en compte les particularité de chacun au niveau du rendu du template du champ.

**libs**

* FormManager
Objet de gestion de formulaire. Ses méthodes permettent de créer un formulaire avec ses différents champs.

* FormHandler
Objet  qui permet de traiter les réponses du formulaire.

**index**

Point d'entré du formulaire et de son traitement.
