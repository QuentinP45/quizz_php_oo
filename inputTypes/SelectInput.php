<?php

// require 'BasicInput.php';

class SelectInput extends BasicInput {
    private $optionValues;
    private $multiple;

    public function __construct( string $type, string $label, string $id, array $options, array $optionValues, bool $multiple)
    {
        parent::__construct($type, $label, $id, $options);
        $this->optionValues = $optionValues;
        $this->multiple = $multiple;
    }

    public function __toString() :string
    {
        return $this->renderField();
    }

    private function renderField() :string
    {
        /* création label */
        $labelField = '<label for="' . $this->id . '">' . $this->label . '</label><br>' . PHP_EOL;

        /* création input */
        $inputField = '<select name="' . $this->id . ($this->multiple ? '[]' : '') . '" id="' . $this->id . '"';
        
        $inputField .= ($this->multiple ? ' multiple="true">' : '>') . PHP_EOL;

        $inputField .= '<option value="">' . ($this->multiple ? '--Choisir vos options--' : '--Choisir une option--') . '</option>' . PHP_EOL;
        
        foreach($this->optionValues as $option){
            $inputField .= '<option value="' . $option . '">' . $option . '</option>' . PHP_EOL;
        }

        $inputField .= '</select><br>'  . PHP_EOL;

        return $labelField . $inputField;
    }
}