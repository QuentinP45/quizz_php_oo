<?php

class CheckboxInput extends BasicInput {
    private $optionValues;

    public function __construct( string $type, string $label, string $id, array $options, array $optionValues)
    {
        parent::__construct($type, $label, $id, $options);
        $this->optionValues = $optionValues;
    }

    public function __toString() :string
    {
        return $this->renderField();
    }

    private function renderField() :string
    {
        /* création label */
        $checkboxField = '<p>' . $this->label . '</p>' . PHP_EOL;

        $i = 0;
        foreach($this->optionValues as $option) {
            ++$i;
            $nameIndex = $this->id . $i; 

            /* création inputs */
            $checkboxField .= '<input type="' . $this->type . '" name="' . $nameIndex . '" id="' . $nameIndex . '" value="' . $option . '">' . PHP_EOL;
            $checkboxField .= '<label for="' . $nameIndex . '">' . $option . '</label><br>' . PHP_EOL;
        }
        
        return $checkboxField;
    }
}
