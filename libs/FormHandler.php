<?php

class FormHandler {
    private $clientAnswers;
    private $waitedAnswers;
    private $score = 0;

    public function __construct(array $clientAnswers, array $waitedAnswers) {
        $this->clientAnswers = $clientAnswers;
        $this->waitedAnswers = $waitedAnswers;
    }

    public function checkResponses() {
        foreach($this->clientAnswers as $label => $answer) {
            if(!empty($this->waitedAnswers[$label])) {
                if(!is_array($answer)){
                    ($answer == $this->waitedAnswers[$label]) ? $this->score += 1 : null;
                } else {
                    foreach($answer as $option) {
                        if(in_array($option, $this->waitedAnswers[$label])) {
                            array_splice($this->waitedAnswers[$label], array_search($option, $this->waitedAnswers[$label]), 1);
                        }
                    }
                    (count($this->waitedAnswers[$label]) == 0) ? $this->score += count($answer) : null;
                }
            }
        }
    }

    public function getScore() {
        return $this->score;
    }
}
