<?php

class BasicInput {
    protected $type;
    protected $label;
    protected $id;
    protected $options;

    public function __construct( string $type, string $label, string $id, array $options)
    {
        $this->type = $type;
        $this->label = $label;
        $this->id = $id;
        $this->options = $options;
    }

    public function __toString() :string
    {
        return $this->renderField();
    }

    private function renderField() :string
    {
        /* création label */
        $labelField = '<label for="' . $this->id . '">' . $this->label . '</label><br>' . PHP_EOL;

        /* création input */
        $inputField = '<input type="' . $this->type . '" name="' . $this->id . '" id="' . $this->id . '"';

        if (!empty($this->options['pattern'])) {
            $inputField .= ' pattern="' . $this->options['pattern'] . '"';
        }

        if ($this->options['required']) {
            $inputField .= ' required';
        }

        if (!empty($this->options['placeholder'])) {
            $inputField .= ' placeholder="' . $this->options['placeholder'] . '"';
        }
        
        $inputField .= '><br>' . PHP_EOL;

        return $labelField . $inputField;
    }
}