<?php

require_once 'inputTypes/BasicInput.php';
require_once 'inputTypes/RangeInput.php';
require_once 'inputTypes/SelectInput.php';
require_once 'inputTypes/CheckboxInput.php';

class FormManager{
    private $inputFields = '';
    
    public function  __construct() {}

    public function openForm(string $method, string $action = '') :string
    {
        return '<form action="' . $action . ' " method="' . $method . '">' . PHP_EOL;
    }

    public function createBasicField(string $type, string $label, string $id, array $options) :void 
    {
        $this->inputFields .= new BasicInput($type, $label, $id, $options);
    }

    public function createRangeField(string $type, string $label, string $id, array $options, int $min, int $max) :void 
    {
        $this->inputFields .= new RangeInput($type, $label, $id, $options, $min, $max);
    }

    public function createSelectField(string $type, string $label, string $id, array $options, array $optionValues, $multiple) :void 
    {
        $this->inputFields .= new SelectInput($type, $label, $id, $options, $optionValues, $multiple);
    }

    public function createCheckboxField(string $type, string $label, string $id, array $options, array $optionValues) :void 
    {
        $this->inputFields .= new CheckboxInput($type, $label, $id, $options, $optionValues);
    }

    public function renderForm()
    {
        return $this->inputFields;
    }

    public function closeForm()
    {
        return $endForm = new BasicInput('submit', '', 'form', ['pattern' => null, 'required' => null, 'placeholder' => null]) . '</form>' . PHP_EOL;
    }
}