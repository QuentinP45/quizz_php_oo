<?php

// require 'BasicInput.php';

class RangeInput extends BasicInput {
    private $min;
    private $max;

    public function __construct( string $type, string $label, string $id, array $options, int $min, int $max)
    {
        parent::__construct($type, $label, $id, $options);
        $this->min = $min;
        $this->max = $max;
    }

    public function __toString() :string
    {
        return $this->renderField();
    }

    private function renderField() :string
    {
        /* création label */
        $labelField = '<label for="' . $this->id . '">' . $this->label . '</label><br>' . PHP_EOL;

        /* création input */
        if(!empty($this->min) && !empty($this->max)) {
            $inputField = '<input type="' . $this->type . '" name="' . $this->id . '" id="' . $this->id . '" min="' . $this->min . '" max="' . $this->max . '"';
        } else {
            $inputField = '<input type="' . $this->type . '" name="' . $this->id . '" id="' . $this->id . '"';
        }
        $inputField .= '><br>' . PHP_EOL;

        return $labelField . $inputField;
    }
}